FROM centos:8
RUN dnf update -y
RUN dnf install -y python38 httpd systemd cairo
COPY ./ /tmp/hack_yuml
COPY web-hack_yuml /var/www/html/
RUN pip3 install /tmp/hack_yuml
RUN echo "ServerName localhost" >> /etc/httpd/conf/httpd.conf
ENV SERVER_HOST=localhost
ENV SERVER_PORT=80
EXPOSE 80
EXPOSE 3000
CMD rm -rf /var/run/httpd/* /run/httpd/* /tmp/httpd* & /usr/sbin/httpd -D FOREGROUND & /tmp/hack_yuml/init_wsgi.sh