#!/usr/bin/env python
from setuptools import setup

with open('README.md', 'r', encoding='utf-8') as f:
    readme = f.read()


setup(name='hack_yuml',
      version="0.0.1",
      description='Aplicacion para yuml',
      long_description=readme,
      long_description_content_type="text/markdown",
      author='Valdr Stiglitz',
      author_email='valdr.stiglitz@gmail.com',
      url='https://github.com/ValdrST/hack-yuml',
      packages=set(['hack_yuml', 'hack_yuml.infraestructura']),
      include_package_data=True,
      install_requires=[i.strip() for i in open("./requirements.txt").readlines()],
      entry_points={
          'console_scripts': ['hack_yuml = hack_yuml:main','hack_yumlWSGI = hack_yuml:wsgi']
      },
      classifiers=[
          'Programming Language :: Python :: 3',
          "Operating System :: OS Independent",
      ])
