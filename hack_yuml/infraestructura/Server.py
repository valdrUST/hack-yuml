#!/usr/bin/env python
from flask import Flask, escape, request, Blueprint, jsonify, make_response, Response
from os import mkdir, path
import requests
import base64
from cairosvg import svg2png
import json
import tempfile
import logging

server_report = Blueprint('app', __name__)
class Server():
    def __init__(self, name, *args, **kwargs):
        self.app = Flask(name)
        self.app.register_blueprint(server_report)

@server_report.route('/ws/hack_yuml',methods=['GET'])
def index():
    return "<h1>hack-yuml index</h1>"

@server_report.route('/ws/hack_yuml/get_diagrama',methods=['POST'])
def get_diagrama():
    datos = request.get_json()
    codigo =  datos["codigo"]
    tipo = datos["tipo"]
    if tipo == "class":
        logging.warning("1111111------111111"+codigo+"1111111------111111")
        r = requests.post("https://yuml.me/diagram/scruffy/class/", data = {'dsl_text':codigo, 'name':""})
        logging.warning(r.text)
        r = requests.post("https://yuml.me/"+r.text)
    elif tipo == "case_use":
        logging.warning(codigo)
        r = requests.post("https://yuml.me/diagram/scruffy/usecase/"+codigo+".svg")
        logging.warning(r.text)
        r = requests.post("https://yuml.me/"+r.text)
    elif tipo == "activity":
        logging.warning(codigo)
        r = requests.post("https://yuml.me/diagram/scruffy/activity/", data = {'dsl_text':codigo, 'name':""})
        logging.warning(r.text)
        r = requests.post("https://yuml.me/"+r.text)
    else:
        logging.error(codigo)
        logging.error(tipo)
        return "error"
    logging.warning(r.content)
    svg2png(bytestring=r.text,write_to="/tmp/diagrama.svg")
    with open("/tmp/diagrama.svg", "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    return encoded_string

@server_report.after_request
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = '*'
    header["Access-Control-Allow-Headers"] = "*"
    header["Access-Control-Allow-Methods"] = "*"
    return response