#!/usr/bin/env python
from .main import application
from waitress import serve

def wsgi():
    serve(application, port="3000",unix_socket_perms='666',ident="hack_yuml",threads=20)
