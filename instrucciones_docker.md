## Manual de instalacion con docker
### Descargar imagen
```bash
docker pull valdrst/landing-page-ec
```
### Correr y crear contenedor
```bash
docker run -dit --name landing-page -p 4001:80 -p 4000:4000 -p 3000:3000 -p 27017:27017 valdrst/landing-page-ec:latest
```
* En el parametro --name se agrega el nombre que tendra el contenedor
* Puerto 80 del contenedor corresponde a un apache
* Puerto 4000 es el backend que se comunica con mongodb
* Puerto 3000 es el frontend de react
* Puerto 27017 es el servidor de mongodb
### Detener contenedor
```bash
docker stop landing-page
```
### Iniciar contenedor
```bash
docker start landing-page
```